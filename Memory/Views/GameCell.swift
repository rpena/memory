//
//  GameCell.swift
//  Memory
//
//  Created by Rey Pena on 9/17/30 H.
//  Copyright © 30 Heisei Rey Pena. All rights reserved.
//

import UIKit

enum FlipState {
    case open
    case close
    
    func toggle() -> FlipState {
        return (self == .open) ? .close : .open
    }
}

class GameCell : UICollectionViewCell, ReusableCell {
   
    private var imageView : UIImageView = UIImageView(frame: .zero)
    private var textLabel : UILabel = UILabel(frame: .zero)
    
    var flipState : FlipState = .close
    
    private var card: Card = Card("a")
    
    private let CARD_BACK_IMAGE : UIImage = #imageLiteral(resourceName: "back_cover")
    
    override func didMoveToWindow() {
        super.didMoveToWindow()
        self.backgroundColor = .clear
        
        imageView.image = CARD_BACK_IMAGE
        imageView.backgroundColor = .white
        imageView.layer.cornerRadius = 5
        imageView.layer.masksToBounds = true
        self.addSubview(imageView)
        imageView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.size.equalToSuperview().multipliedBy(0.95)
        }
        
        textLabel.font = UIFont.systemFont(ofSize: self.frame.width * 0.5)
        textLabel.textColor = .white
        textLabel.textAlignment = .center
        textLabel.adjustsFontSizeToFitWidth = true
        self.addSubview(textLabel)
        textLabel.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.size.equalToSuperview().dividedBy(1.5)
        }
    }
    
    public func config(card: Card) {
        self.card = card
        
        guard !card.isMatched else {
            imageView.backgroundColor = .clear
            imageView.image = nil
            textLabel.text = nil
            return
        }
        
        flipState = card.isOpen ? .open : .close
        flip(flipState)
    }
    
    private func flip(_ state: FlipState) {
        guard !card.isMatched else { return }
        imageView.backgroundColor = .darkGray
        
        self.flipState = state
        self.isUserInteractionEnabled = false

        if state == .open {
            
            imageView.image = nil
            UIView.transition(with: imageView, duration: 0.3, options: .transitionFlipFromLeft, animations: nil, completion: { [unowned self] (finish) in
                self.textLabel.text = "\(self.card.value)"
                self.isUserInteractionEnabled = true
            })
            
        } else {
            
            imageView.image = CARD_BACK_IMAGE
            textLabel.text = nil
            UIView.transition(with: imageView, duration: 0.3, options: .transitionFlipFromRight, animations: nil, completion: { (finished) in
                self.isUserInteractionEnabled = true
            })
            
        }
    }
    
    override var isSelected: Bool {
        didSet {
            guard isSelected else { return }
            self.flip(self.flipState.toggle())
        }
    }
    
}
