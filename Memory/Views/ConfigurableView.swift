//
//  ConfigurableView.swift
//  Memory
//
//  Created by Rey Pena on 9/17/30 H.
//  Copyright © 30 Heisei Rey Pena. All rights reserved.
//

import Foundation
import UIKit

protocol ReusableCell {
    static var identifier : String {get}
}

extension ReusableCell where Self : UIView {
    static var identifier : String {
        return NSStringFromClass(self)
    }
}

extension UICollectionView {
    func register<T: UICollectionViewCell>(cell: T.Type) where T: ReusableCell {
        register(T.self, forCellWithReuseIdentifier: T.identifier)
    }
    
    func dequeueReusableCell<T: UICollectionViewCell>(for indexPath: IndexPath) -> T where T : ReusableCell {
        
        guard let cell = dequeueReusableCell(withReuseIdentifier: T.identifier, for: indexPath) as? T else {
            fatalError("Could not dequeue \(T.identifier)")
        }
        return cell
    }
}
