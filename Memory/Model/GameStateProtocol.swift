//
//  GameStateProtocol.swift
//  Memory
//
//  Created by Rey Pena on 9/18/30 H.
//  Copyright © 30 Heisei Rey Pena. All rights reserved.
//

import Foundation

enum GameState {
    static let startSelection : String = "Pick a card"
    static let nextSelection : String = "Pick another card"
    static let gameFinished : String = "Game Over"
}

protocol GameStateProtocol : class {
    func gameStatusDidChange(game: Game, state: String)
}
