//
//  Card.swift
//  Memory
//
//  Created by Rey Pena on 9/17/30 H.
//  Copyright © 30 Heisei Rey Pena. All rights reserved.
//

import Foundation

struct Card : Equatable {
    
    var value: Character
    var isOpen: Bool = false
    var isMatched : Bool = false
    
    init(_ value: Character) {
        self.value = value
    }
    
    static func ==(lhs: Card, rhs: Card) -> Bool {
        return lhs.value == rhs.value
    }
    
}
