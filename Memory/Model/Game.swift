//
//  Game.swift
//  Memory
//
//  Created by Rey Pena on 9/17/30 H.
//  Copyright © 30 Heisei Rey Pena. All rights reserved.
//

import Foundation
import UIKit

enum GameMode {
    case m_2x2
    case m_4x4
    case m_6x6
    
    static let all : [GameMode] = [.m_2x2,.m_4x4,.m_6x6]
    
    var difficulty : String {
        switch self {
        case .m_2x2:
            return "Easy"
        case .m_4x4:
            return "Moderate"
        case .m_6x6:
            return "Hard"
        }
    }
    
    var dimension : (vertical: CGFloat, horizontal: CGFloat) {
        switch self {
        case .m_2x2:
            return (vertical: 2, horizontal: 2)
        case .m_4x4:
            return (vertical: 4, horizontal: 4)
        case .m_6x6:
            return (vertical: 6, horizontal: 6)
        }
    }
    
    var cardCount : Int {
        return Int(self.dimension.horizontal * self.dimension.vertical)
    }
}

struct GameConfig {
    var spacing : CGFloat = 5
    var mode : GameMode
    var player: Player
    
    init(mode: GameMode, player: String) {
        self.mode = mode
        self.player = Player(name: player)
    }
}


struct Game {
        
    var deck : [Card] = []
    var rounds : Int = 0
    var config: GameConfig
    
    var isFinished : Bool {
        return self.deck.filter({ $0.isMatched == false }).count == 0
    }
    
    var ranking : String {
        let deckCount = Double(self.deck.count)
        if self.rounds <= Int(deckCount * 0.5) {
            return "⭐️⭐️⭐️"
        } else if self.rounds <= Int(deckCount * 0.75) {
             return "⭐️⭐️"
        } else {
             return "⭐️"
        }
    }
        
    init(config: GameConfig) {
        self.config = config
        self.deck = populate(with: config)
    }
    
    private mutating func populate(with config: GameConfig) -> [Card] {
        var letters = Array("abcdefghijklmnopqrstuvwxyz")
        var deck : [Card] = []
        (0..<config.mode.cardCount / 2).forEach({
            deck.append(Card(letters[$0]))
            deck.append(Card(letters[$0]))
        })
        return deck
    }
    
    private mutating func toggleCard(at index: Int) {
        guard var card : Card = deck[safe: index] else {return}
        
        card.isOpen = !card.isOpen
        deck[index] = card
    }
    
    mutating func resetGame() {
        self.deck = populate(with: config)
        self.deck.shuffle()
        self.rounds = 0
    }
        
    mutating func incrementRound() {
        rounds += 1
    }
    
}
