//
//  Control.swift
//  Memory
//
//  Created by Rey Jenald Pena on 2018/09/18.
//  Copyright © 2018 Rey Pena. All rights reserved.
//

import Foundation

typealias CardSelection = (index: Int, card: Card)

class GameControl {
    
    var checkerArray: [CardSelection] = []
    
    weak var delegate : GameStateProtocol?
    
    var game : Game
    var deckCount : Int {
        return game.deck.count
    }
    
    var currentPlayer : String {
        return game.config.player.name
    }
    
    init(game: Game) {
        self.game = game
        self.game.deck.shuffle()
    }
        
    func isMatched() -> Bool {
        guard let cardA : CardSelection = checkerArray[safe: 0] else {return false}
        guard let cardB : CardSelection = checkerArray[safe: 1] else {return false}
        
        return cardA.card == cardB.card
    }
    
    func updateCardState(on indices: [Int], as matched: Bool) {
        for index in indices {
            game.deck[index].isMatched = matched
        }
    }
    
    func appendToSelection(_ cardSelection: CardSelection) {
        guard self.checkerArray.count < 2 else {return}
        self.checkerArray.append(cardSelection)
    }
    
    private func validateMatchOnSelections() -> [Int] {
        
        // incement round
        game.incrementRound()
        
        // check if selected cards match
        let isMatch = self.isMatched()
        let indices = checkerArray.map({return $0.index})
        
        // update cards states
        updateCardState(on: indices, as: isMatch)
        
        // remove all selections
        checkerArray.removeAll()
        
        return indices
    }
    
    private func updateGameSate() {
        
        guard !game.isFinished else {
            self.delegate?.gameStatusDidChange(game: game, state: GameState.gameFinished)
            return
        }
        
        switch self.checkerArray.count {
            case 0:
                self.delegate?.gameStatusDidChange(game: game, state: GameState.startSelection)
            case 1:
                self.delegate?.gameStatusDidChange(game: game, state: GameState.nextSelection)
            default:
                break
        }
    }
    
    func resetGame() {
        self.game.resetGame()
    }
    
    func check(cardAt index: Int, reloadIndices: (_ indices: [Int]) ->()) {
        
        guard let card : Card = game.deck[safe: index] else {return}
        guard !card.isMatched else {return}
        
        // unselect card if already open
        if let existingIndex = checkerArray.index(where: { $0.index == index}) {
            game.incrementRound()
            checkerArray.remove(at: existingIndex)
            self.updateGameSate()
            return
        }
        
        // append card to selection array for checking
        let newSelection = CardSelection(index: index, card: card)
        self.appendToSelection(newSelection)
        
        // when 2 cards are on selection array
        if checkerArray.count == 2 {
            
            // validate selections if matched
            let indices = self.validateMatchOnSelections()
            reloadIndices(indices)
        }
        
        // inform user of state
        self.updateGameSate()
    }
}
