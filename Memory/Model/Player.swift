//
//  Player.swift
//  Memory
//
//  Created by Rey Jenald Pena on 2018/09/19.
//  Copyright © 2018 Rey Pena. All rights reserved.
//

import Foundation

struct Player {
    
    var name : String
    var scores : [Int] = []
    
    init(name: String) {
        self.name = name
    }
}
