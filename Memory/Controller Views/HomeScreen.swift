//
//  HomeScreen.swift
//  Memory
//
//  Created by Rey Jenald Pena on 2018/09/19.
//  Copyright © 2018 Rey Pena. All rights reserved.
//

import Foundation
import UIKit

extension HomeViewController {
    
    func layoutUserInterface() {
        
        self.view.backgroundColor = .white
        
        let titleLabel = UILabel(frame: .zero)
        titleLabel.text = "MEMORY"
        titleLabel.textColor = .darkGray
        titleLabel.font = UIFont(name: Fonts.HelveticaNeueUltraLight, size: 30)
        titleLabel.textAlignment = .center
        self.view.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview().multipliedBy(0.5)
        }
        
        playerTextField.font = UIFont(name: Fonts.HelveticaNeueLight, size: 16)
        playerTextField.textAlignment = .center
        playerTextField.placeholder = "Enter your name here"
        playerTextField.borderStyle = .roundedRect
        playerTextField.delegate = self
        self.view.addSubview(playerTextField)
        playerTextField.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview().multipliedBy(1.25)
            make.width.equalToSuperview().dividedBy(1.5)
            make.height.equalTo(40)
        }
        
        let imageView = UIImageView(image: #imageLiteral(resourceName: "thinking"))
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFit
        self.view.addSubview(imageView)
        imageView.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom)
            make.bottom.equalTo(playerTextField.snp.top)
            make.centerX.equalToSuperview()
        }

        
        let startButton : UIButton = UIButton(type: .system)
        startButton.addTarget(self, action: #selector(didTapStartButton), for: .touchUpInside)
        startButton.layer.cornerRadius = 5
        startButton.layer.masksToBounds = true
        startButton.setTitle("Start", for: .normal)
        startButton.backgroundColor = self.view.tintColor
        startButton.setTitleColor(.white, for: .normal)
        self.view.addSubview(startButton)
        startButton.snp.makeConstraints { (make) in
            make.top.equalTo(playerTextField.snp.bottom).offset(15)
            make.centerX.equalTo(playerTextField)
            make.width.equalTo(playerTextField)
            make.height.equalTo(playerTextField).multipliedBy(1.5)
        }
    }
}
