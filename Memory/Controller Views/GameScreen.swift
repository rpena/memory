//
//  GameScreen.swift
//  Memory
//
//  Created by Rey Pena on 9/17/30 H.
//  Copyright © 30 Heisei Rey Pena. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

extension GameViewController {
    
    func layoutUserInterface() {
        
        self.view.backgroundColor = .white
        
        let config : GameConfig = self.gameControl.game.config
        
        let cardWidth : CGFloat = self.view.frame.width / config.mode.dimension.horizontal
        let boardHeight : CGFloat = cardWidth * config.mode.dimension.vertical
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.itemSize = CGSize(width: cardWidth, height: cardWidth)
        flowLayout.minimumLineSpacing = 0
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.sectionInset = .zero
        
        boardCollectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        boardCollectionView?.backgroundColor = .white
        boardCollectionView?.isScrollEnabled = false
        self.view.addSubview(boardCollectionView!)
        boardCollectionView?.snp.makeConstraints({ (make) in
            make.center.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalTo(boardHeight)
        })
        
        statusLabel = UILabel(frame: .zero)
        statusLabel?.textColor = .black
        statusLabel?.font = UIFont(name: Fonts.HelveticaNeueLight, size: 16)
        statusLabel?.adjustsFontSizeToFitWidth = true
        statusLabel?.text = GameState.startSelection
        self.view.addSubview(statusLabel!)
        statusLabel?.snp.makeConstraints({ (make) in
            make.left.equalTo(boardCollectionView!).offset(10)
            make.bottom.equalTo(boardCollectionView!.snp.top).offset(-10)
            make.height.equalTo(20)
            make.width.lessThanOrEqualTo(boardCollectionView!)
        })
        
        boardCollectionView?.dataSource = self
        boardCollectionView?.delegate = self
        boardCollectionView?.register(cell: GameCell.self)
        
    }
    
}
