//
//  AppStyling.swift
//  Memory
//
//  Created by Rey Pena on 9/19/30 H.
//  Copyright © 30 Heisei Rey Pena. All rights reserved.
//

import Foundation

enum Fonts {
    static let HelveticaNeueLight : String = "HelveticaNeue-Light"
    static let HelveticaNeueUltraLight : String = "HelveticaNeue-UltraLight"
    static let HelveticaNeueBold : String = "HelveticaNeue-Bold"
    static let HelveticaNeueMedium : String = "HelveticaNeue-Medium"
}
