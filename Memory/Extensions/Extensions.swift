//
//  Extensions.swift
//  Memory
//
//  Created by Rey Jenald Pena on 2018/09/18.
//  Copyright © 2018 Rey Pena. All rights reserved.
//

import Foundation
import UIKit

extension Array {
    subscript (safe index: Int) -> Element? {
        return indices ~= index ? self[index] : nil
    }
    
    mutating func shuffle() {
        guard self.count > 2 else {return}
        
        var newList = self
        for i in 0..<(newList.count - 1) {
            let randInt = Int(arc4random_uniform(UInt32(newList.count - 1)))
            newList.swapAt(i, randInt)
        }
        
        self = newList
    }
}
