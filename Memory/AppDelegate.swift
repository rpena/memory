//
//  AppDelegate.swift
//  Memory
//
//  Created by Rey Pena on 9/17/30 H.
//  Copyright © 30 Heisei Rey Pena. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        let window = UIWindow()
        window.makeKeyAndVisible()
        
        let mainNav = UINavigationController(rootViewController: HomeViewController())
        window.rootViewController = mainNav
        
        self.window = window
        
        return true
    }

}

