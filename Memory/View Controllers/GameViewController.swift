//
//  GameViewController.swift
//  Memory
//
//  Created by Rey Pena on 9/17/30 H.
//  Copyright © 30 Heisei Rey Pena. All rights reserved.
//

import UIKit
import Async

class GameViewController: UIViewController {
    
    var boardCollectionView : UICollectionView?
    var statusLabel : UILabel?
    
    var gameControl : GameControl
    
    init(game: Game) {
        self.gameControl = GameControl(game: game)
        super.init(nibName: nil, bundle: nil)
        self.gameControl.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.layoutUserInterface()
        let quitButton = UIBarButtonItem(title: "Give up", style: .plain, target: self, action: #selector(quitGame))
        quitButton.tintColor = .red
        self.navigationItem.leftBarButtonItem = quitButton
        
        self.navigationItem.title = "Go \(self.gameControl.currentPlayer)!"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }

    
    @objc private func quitGame() {
        
        let alert = UIAlertController(title: "Come on \(self.gameControl.currentPlayer)!", message: "Are you really giving up?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Yup", style: .destructive, handler: { (action) in
            self.navigationController?.popViewController(animated: true)
        }))
        
        alert.addAction(UIAlertAction(title: "Naah! Just kiddin'", style: .default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension GameViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.gameControl.deckCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let card : Card = self.gameControl.game.deck[indexPath.row]
        
        let cell : GameCell = collectionView.dequeueReusableCell(for: indexPath)
        cell.config(card: card)
        
        return cell
    }
    
}

extension GameViewController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        gameControl.check(cardAt: indexPath.row) { [unowned self] (indices) in
            Async.main(after: 0.7, {
                let indexPaths = indices.map({return IndexPath(row: $0, section: 0)})
                for path in indexPaths {
                    
                    let card : Card = self.gameControl.game.deck[path.row]
                    let cell = collectionView.cellForItem(at: path) as? GameCell
                    cell?.config(card: card)
                    
                }
            })
        }
        
    }
    
}

extension GameViewController : GameStateProtocol {
    
    func gameStatusDidChange(game: Game, state: String) {
        
        if state == GameState.gameFinished {
            
            statusLabel?.text = nil
            
            Async.main(after: 1.0, {
                let alert = UIAlertController(title: "\(self.gameControl.game.ranking)", message: "\nNice one \(self.gameControl.currentPlayer)! \nYou finished the game in \(game.rounds) rounds", preferredStyle: .alert)
                let resetAction = UIAlertAction(title: "Play Again", style: .default, handler: { [unowned self] (action) in
                    self.gameControl.resetGame()
                    self.boardCollectionView?.reloadData()
                    self.statusLabel?.text = GameState.startSelection
                })
                alert.addAction(resetAction)
                
                alert.addAction(UIAlertAction(title: "I'll take a break", style: .destructive, handler: { (action) in
                    self.navigationController?.popViewController(animated: true)
                }))

                self.present(alert, animated: true, completion: nil)
            })
            
        } else {
            self.statusLabel?.text = state
        }
        
    }
}
