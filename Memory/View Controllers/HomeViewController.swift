//
//  HomeViewController.swift
//  Memory
//
//  Created by Rey Jenald Pena on 2018/09/19.
//  Copyright © 2018 Rey Pena. All rights reserved.
//

import Foundation
import UIKit
import Async

class HomeViewController : UIViewController {
    
    private let levels : [GameMode] = GameMode.all
    let playerTextField : UITextField = UITextField(frame: .zero)
    
    init() {
        super.init(nibName: nil, bundle: nil)
        self.layoutUserInterface()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @objc func didTapStartButton() {
        
        let levelAlert = UIAlertController(title: "Choose A Level", message: nil, preferredStyle: .alert)
        
        // create an alert action for each level
        for level in levels {
            
            levelAlert.addAction(UIAlertAction(title: level.difficulty, style: .default, handler: { [unowned self] (action) in
                print(level.difficulty)
                
                // check if player name is not nil and has text
                guard let playerName : String = self.playerTextField.text, playerName.count > 0 else {
                    let alert = UIAlertController(title: "Oops!", message: "Please tell me your name before we play.", preferredStyle: .alert)
                    alert.addAction((UIAlertAction(title: "Got it", style: .default, handler: nil)))
                    self.present(alert, animated: true, completion: nil)
                    return
                }
                
                // create instance of game viewcontroller with config and player name
                let gameConfig = GameConfig(mode: level, player: playerName)
                let game = Game(config: gameConfig)
                let gameController = GameViewController(game: game)

                // present game vc on delay
                Async.main(after: 0.8, { [unowned self] in
                    self.navigationController?.pushViewController(gameController, animated: true)
                })
                
            }))
        }
        
        self.present(levelAlert, animated: true, completion: nil)
    }
}

extension HomeViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
