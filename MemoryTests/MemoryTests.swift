//
//  MemoryTests.swift
//  MemoryTests
//
//  Created by Rey Pena on 9/17/30 H.
//  Copyright © 30 Heisei Rey Pena. All rights reserved.
//

import XCTest
@testable import Memory

class MemoryTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    func testShuffleInt() {
        var intArray = [1,2,3,4,5,6,7,8,9,10]
        intArray.shuffle()
        
        XCTAssertNotEqual(intArray, [1,2,3,4,5,6,7,8,9,10])
    }
    
    func testShuffleString() {
        var stringArray = Array("abcdefghijklmnop")
        stringArray.shuffle()
        
        XCTAssertNotEqual(stringArray, Array("abcdefghijklmnop"))
    }
    
}
