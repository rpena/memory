//
//  GameControlTests.swift
//  MemoryTests
//
//  Created by Rey Jenald Pena on 2018/09/19.
//  Copyright © 2018 Rey Pena. All rights reserved.
//

import XCTest
@testable import Memory

class GameControlTests: XCTestCase {
    
    let game = Game(config: GameConfig(mode: .m_2x2, player: "Test"))
    var gameControl : GameControl!
    
    override func setUp() {
        super.setUp()
        self.gameControl = GameControl(game: game)
    }

    func testGameControl_PlayerName() {
        XCTAssertEqual(self.gameControl.currentPlayer, "Test")
    }
    
    func testGameControl_SetCardsMatch() {
        self.gameControl.updateCardState(on: [0], as: true)
        
        XCTAssertTrue(self.gameControl.game.deck[0].isMatched)
        
        self.gameControl.updateCardState(on: [0], as: false)
        
        XCTAssertFalse(self.gameControl.game.deck[0].isMatched)
    }
    
    func testGameControl_AppendSelection() {
        
        let selection1 = CardSelection(index: 0, card: self.gameControl.game.deck[0])
        let selection2 = CardSelection(index: 1, card: self.gameControl.game.deck[1])
        let selection3 = CardSelection(index: 2, card: self.gameControl.game.deck[2])

        self.gameControl.appendToSelection(selection1)
        self.gameControl.appendToSelection(selection2)
        self.gameControl.appendToSelection(selection3)
        
        XCTAssertEqual(self.gameControl.checkerArray[0].card, self.gameControl.game.deck[0])
        XCTAssertEqual(self.gameControl.checkerArray[1].card, self.gameControl.game.deck[1])
        XCTAssertNil(self.gameControl.checkerArray[safe: 2])

    }
    
    func testGameControl_MatchingChecker() {
        
        let selection1 = CardSelection(index: 0, card: self.gameControl.game.deck[0])
        self.gameControl.appendToSelection(selection1)
        
        XCTAssertFalse(self.gameControl.isMatched())
        
    }
    
    func testGameControl_CardSelection() {
        
        let selection1 = CardSelection(index: 0, card: self.gameControl.game.deck[0])
        self.gameControl.appendToSelection(selection1)

        self.gameControl.check(cardAt: 1) { (indices) in
            XCTAssertEqual(indices, [0,1])
        }
        
    }

}

